import java.util.ArrayList;
public class Task5120 {
    public static void main(String[] args) throws Exception {
        //task 1
        ArrayList<String> colors = new ArrayList<String>();
        colors.add("yellow");
        colors.add("red");
        colors.add("blue");
        colors.add("orange");
        colors.add("pink");
        System.out.println("Task 1: " + colors);
        //task 2
          ArrayList<Integer> array1 = new ArrayList<Integer>();
          array1.add(10);
          array1.add(15);
          array1.add(20);
          System.out.println("Task 2 - array 1: " + array1);
          ArrayList<Integer> array2 = new ArrayList<Integer>();
          array2.add(-1);
          array2.add(7);
          array2.add(-8);
          System.out.println("Task 2 - array 2: " + array2);
          ArrayList<Integer> sumArrayList = new ArrayList<Integer>();
          for (int i=0; i < array1.size(); i++) {
            sumArrayList.add(array1.get(i) + array2.get(i));
          }
          System.out.println("Task 2 - array sum: " + sumArrayList);
          //task 3
          ArrayList<String> colors2 = new ArrayList<String>();
          colors2.add("yellow");
          colors2.add("red");
          colors2.add("blue");
          colors2.add("orange");
          colors2.add("pink");
          int count = 0;
          for (int i=0; i < colors2.size(); i++){
            count++;
          }
          System.out.println("Task 3 - number of color: " + count);
          //task 4
          System.out.println("Task 4 - 4th color is: " + colors2.get(3));
          //task 5
          ArrayList<String> colors3 = new ArrayList<String>();
          colors3.add("white");
          colors3.add("red");
          colors3.add("blue");
          colors3.add("green");
          colors3.add("purple");
          int count2 = 0;
          for (int i=0; i < colors3.size(); i++){
            count2++;
          }
          System.out.println("Task 5 - " + count2 + "th color is: " + colors3.get(count2 - 1));
          //task 6
          ArrayList<String> colors4 = new ArrayList<String>();
          colors4.add("white");
          colors4.add("red");
          colors4.add("blue");
          colors4.add("green");
          colors4.add("purple");
          System.out.println("Task 6 - original array: " + colors4);
          int count3 = 0;
          for (int i=0; i < colors4.size(); i++){
            count3++;
          }
          colors4.remove(count3 - 1);
          System.out.println("Task 6 - new array: " + colors4);
          //task 7
          ArrayList<String> colors7 = new ArrayList<String>();
          colors7.add("white");
          colors7.add("red");
          colors7.add("blue");
          colors7.add("green");
          colors7.add("purple");
          System.out.println("Task 7 - print array element forEach: " + colors7);
          colors7.forEach((n) -> System.out.println(n));
          //task 8
          System.out.println("Task 8 - print array element: " 
          + colors7.get(0) + " "
          + colors7.get(1) + " "
          + colors7.get(2) + " "
          + colors7.get(3) + " "
          + colors7.get(4) + " ");
          //task 9
          for (int i = 0; i < colors7.size(); i++){
            System.out.println("Task 9 - print array element: " + colors7.get(i) + " ");
          }
          //task 10
          colors7.add(0,"black");
          System.out.println("Task 10 - print array element: " + colors7);
          //task 11
          colors7.set(2,"yellow");
          System.out.println("Task 11 - change 3rd element to yellow: " + colors7);
          //task 12
          ArrayList<String> persons12 = new ArrayList<String>();
          persons12.add("John");
          persons12.add("Alice");
          persons12.add("Bob");
          persons12.add("Steve");
          persons12.add("John");
          persons12.add("Steve");
          persons12.add("Maria");
          System.out.println("Task 12 - index of Alice: " + persons12.indexOf("Alice"));
          System.out.println("Task 12 - index of Mark: " + persons12.indexOf("Mark"));
          //task 13
          System.out.println("Task 13 - last index of Steve: " + persons12.lastIndexOf("Steve"));
          System.out.println("Task 13 - index of John: " + persons12.lastIndexOf("John"));
    }
}
